# build stage
FROM node:16.13-alpine as build-stage
WORKDIR /app
COPY . .
RUN yarn install
RUN yarn run build

# production stage
FROM nginx:1.20-alpine as production-stage
COPY --from=build-stage /app/build /usr/share/nginx/html
WORKDIR /usr/share/nginx/html
COPY ./env.sh .
COPY .env .
# Add bash
RUN apk add --no-cache bash

# Make our shell script executable
RUN chmod +x env.sh

# Start Nginx server
CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
