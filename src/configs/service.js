const API_URL = process.env.API_URL 
const config = {
  development: {
    url: API_URL
  },
  production: {
    url: API_URL
  },
  test: {
    url: API_URL
  }
}

const nodeEnv = process.env.NODE_ENV 

export default config[nodeEnv]
